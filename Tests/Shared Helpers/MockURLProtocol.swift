//
//  MockURLProtocol.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/19/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


//References:
//  --: https://www.hackingwithswift.com/articles/153/how-to-test-ios-networking-code-the-easy-way
//  --: https://nshipster.com/nsurlprotocol/
@objc class MockURLProtocol: URLProtocol {

    /// maps URLs to test data
    static var testURLs = [URL?: Data]()

    static var response: URLResponse?
    static var error: Error?


    // Handle all types of requests
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }

    // Handle all types of tasks
    override class func canInit(with task: URLSessionTask) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }


    override func startLoading() {
        // if we have a valid URL…
        if let url = request.url {
            // …and if we have test data for that URL…
            if let data = Self.testURLs[url] {
                // …load it immediately.
                self.client?.urlProtocol(self, didLoad: data)
            }
        }

        // …and we return our response if defined…
        if let response = Self.response {
            self.client?.urlProtocol(self,
                                     didReceive: response,
                                     cacheStoragePolicy: .notAllowed)
        }

        // …and we return our error if defined…
        if let error = Self.error {
            self.client?.urlProtocol(self, didFailWithError: error)
        }
        // mark that we've finished
        self.client?.urlProtocolDidFinishLoading(self)
    }


    // this method is required but doesn't need to do anything
    override func stopLoading() {

    }
}
