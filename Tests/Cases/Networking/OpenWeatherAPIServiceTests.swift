//
//  OpenWeatherAPIServiceTests.swift
//  OpenWeatherAPIServiceTests
//
// Created by Brian Sipple on 4/17/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import XCTest
import Combine
@testable import TimelessWeather


class OpenWeatherAPIServiceTests: XCTestCase {
    private var subscriptions = Set<AnyCancellable>()

    private var sut: OpenWeatherAPIService!

    private var location: WeatherLocation!
    private var subscriptionQueue: DispatchQueue!
    private var mockSession: URLSession!
    private var apiKey: String!

    let validResponse = HTTPURLResponse(
        url: URL(string: "https://api.openweathermap.org/data/2.5/onecall")!,
        statusCode: 200,
        httpVersion: nil,
        headerFields: nil
    )
}


// MARK: - Lifecycle
extension OpenWeatherAPIServiceTests {

    override func setUpWithError() throws {
        // Put setup code here.
        // This method is called before the invocation of each
        // test method in the class.
        super.setUp()

        location = WeatherLocation(latitude: 41.85, longitude: -87.65)
        mockSession = makeMockSession()
        subscriptionQueue = DispatchQueue(label: "Test API Queue", qos: .userInitiated)
        apiKey = "TEST_API_KEY"

        sut = makeSUT()
    }


    override func tearDownWithError() throws {
        // Put teardown code here.
        // This method is called after the invocation of each
        // test method in the class.
        location = nil
        sut = nil
        subscriptionQueue = nil
        mockSession = nil
        apiKey = nil

        MockURLProtocol.response = nil
        MockURLProtocol.error = nil
        MockURLProtocol.testURLs = [URL? : Data]()

        super.tearDown()
    }
}


// MARK: - Factories
extension OpenWeatherAPIServiceTests {

    func makeMockSession() -> URLSession {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]

        return URLSession(configuration: config)
    }


    func makeSUTWithDefaults() -> OpenWeatherAPIService {
        .init(apiKey: apiKey)
    }


    func makeSUT(
        session: URLSession? = nil,
        subscriptionQueue: DispatchQueue? = nil,
        apiKey: String? = nil
    ) -> OpenWeatherAPIService {
        .init(
            session: session ?? self.mockSession,
            subscriptionQueue: subscriptionQueue ?? self.subscriptionQueue,
            apiKey: apiKey ?? self.apiKey
        )
    }
}


// MARK: - "Given" Helpers (Conditions Exist)
private extension OpenWeatherAPIServiceTests {

    func givenSomething() {
        // some state or condition is established
    }
}


// MARK: - "When" Helpers (Actions Are Performed)
private extension OpenWeatherAPIServiceTests {

    func whenSomethingHappens() {
        // perform some action
    }
}


// MARK: - Test Initial Conditions
extension OpenWeatherAPIServiceTests {

    func test_apiService_whenCreatedWithDefaults_usesDefaultResponseQueueLabel() throws {
        sut = makeSUTWithDefaults()

        XCTAssertEqual(sut.subscriptionQueue.label, OpenWeatherAPIService.defaultSubscriptionQueueLabel)
    }


    func test_apiService_whenCreatedWithDefaults_usesDefaultURLSessionForPublisher() throws {
        sut = makeSUTWithDefaults()

        XCTAssertEqual(sut.taskPublisher.session, .shared)
    }


    func test_apiService_whenCreatedWithDefaults_usesDefaultAPIKey() throws {
        sut = makeSUTWithDefaults()

        XCTAssertEqual(sut.apiKey, apiKey)
    }


    func test_apiService_whenCreatedWithSession_makesPublisherWithSession() throws {
         XCTAssertEqual(sut.taskPublisher.session, mockSession)
    }


    func test_sharedAPIService_usesSharedURLSessionForTaskPublisher() throws {
        XCTAssertEqual(OpenWeatherAPIService.shared.taskPublisher.session, .shared)
    }
}



// MARK: - Test Fetching Forecasts
extension OpenWeatherAPIServiceTests {

    func test_makeRequestPublisher_usesCorrectURL() throws {
        let taskPublisher = sut.makeRequestPublisher(for: location, using: apiKey)

        XCTAssertEqual(taskPublisher?.request.url?.host, Endpoint.OpenWeatherAPI.host)
    }

// ------------------------------------------------------------------------
// TODO: Add better test coverage for requests and request handling.
// ------------------------------------------------------------------------

    func test_fetchCurrentForecast_givenValidJSON_emitsCurrentForecastModel() throws {
        let url = Endpoint.OpenWeatherAPI.oneCall(for: location, using: apiKey).url!
        let expectation = self.expectation(description: "Current Forecast Model should be emitted.")

        MockURLProtocol.testURLs[url] = try Data.fromJSON(fileName: "GET_OneCall_Chicago")
        MockURLProtocol.response = validResponse

        
        let _ = sut
            .fetchCurrentWeatherForecast(for: location)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        XCTFail(error.errorDescription ?? "fetchCurrentWeatherForecast Error")
                    case .finished:
                        break
                    }
                },
                receiveValue: { forecast in
                    XCTAssertNotNil(forecast)
                    expectation.fulfill()
                }
            )
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }

    
    func test_fetchHourlyForecast_givenValidJSON_emitsHourlyForecastModel() throws {
        let url = Endpoint.OpenWeatherAPI.oneCall(for: location, using: apiKey).url!
        let expectation = self.expectation(description: "Current Forecast Model should be emitted.")

        MockURLProtocol.testURLs[url] = try Data.fromJSON(fileName: "GET_OneCall_Chicago")
        MockURLProtocol.response = validResponse

        
        let _ = sut
            .fetchHourlyWeatherForecast(for: location)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        XCTFail(error.errorDescription ?? "fetchHourlyWeatherForecast Error")
                    case .finished:
                        break
                    }
                },
                receiveValue: { forecast in
                    XCTAssertNotNil(forecast)
                    expectation.fulfill()
                }
            )
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }


    func test_fetchDailyForecast_givenValidJSON_emitsDailyForecastModel() throws {
        let url = Endpoint.OpenWeatherAPI.oneCall(for: location, using: apiKey).url!
        let expectation = self.expectation(description: "Current Forecast Model should be emitted.")

        MockURLProtocol.testURLs[url] = try Data.fromJSON(fileName: "GET_OneCall_Chicago")
        MockURLProtocol.response = validResponse

        
        let _ = sut
            .fetchDailyWeatherForecast(for: location)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        XCTFail(error.errorDescription ?? "fetchDailyWeatherForecast Error")
                    case .finished:
                        break
                    }
                },
                receiveValue: { forecast in
                    print(forecast)
                    XCTAssertNotNil(forecast)
                    expectation.fulfill()
                }
            )
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }

    
    func test_fetchForecast_givenValidJSON_emitsError() throws {
    }
}



