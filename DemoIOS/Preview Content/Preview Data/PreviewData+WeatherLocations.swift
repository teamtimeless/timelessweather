//
//  PreviewData+WeatherLocations.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation
import TimelessWeather


extension PreviewData {

    enum WeatherLocations {
        static let chicago = WeatherLocation(latitude: 41.85, longitude: -87.65)
    }
}
