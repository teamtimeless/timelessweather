//
//  CurrentWeatherOnlyViewModel.swift
//  DemoIOS
//
//  Created by William Starr on 7/1/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI
import Combine
import TimelessWeather


extension CurrentWeatherOnlyExample {
    final class ViewModel: ObservableObject {
        private var subscriptions = Set<AnyCancellable>()

        let weatherLocation: WeatherLocation?
        let openWeatherAPIService: OpenWeatherAPIServicing

        lazy var temperatureFormatter: MeasurementFormatter = makeMeasurementFormatter()

        // MARK: - Published Outputs
        @Published var currentWeatherForecast: CurrentWeatherOnlyForecast?


        // MARK: - Init
        init(
            weatherLocation: WeatherLocation?,
            openWeatherAPIService: OpenWeatherAPIServicing = OpenWeatherAPIService.shared
        ) {
            self.weatherLocation = weatherLocation
            self.openWeatherAPIService = openWeatherAPIService

            self.openWeatherAPIService.apiKey = AppCredentials.OpenWeatherAPI.apiKey

            setupSubscribers()
        }
    }
}


// MARK: - Publishers
extension CurrentWeatherOnlyExample.ViewModel {

    private var someValuePublisher: AnyPublisher<String, Never> {
        Just("")
            .eraseToAnyPublisher()
    }
}


// MARK: - Computeds
extension CurrentWeatherOnlyExample.ViewModel {

    var currentTemperatureFahrenheit: Double? {
        currentWeatherForecast?.temperature.converted(to: .fahrenheit).value
    }

    var currentTemperatureCelsius: Double? {
        currentWeatherForecast?.temperature.converted(to: .celsius).value
    }

    var currentTemperatureText: String {
        guard let currentTemperature = currentWeatherForecast?.temperature else { return "N/A" }

        return temperatureFormatter.string(from: currentTemperature)
    }

    var conditionText: String {
        currentWeatherForecast?.primaryCondition?.rawValue ?? "N/A"
    }
}


// MARK: - Public Methods
extension CurrentWeatherOnlyExample.ViewModel {

    func fetchCurrentWeather() {
        openWeatherAPIService.fetchCurrentWeatherOnlyForecast(
            for: weatherLocation
        )
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { completion in

            },
            receiveValue: { [weak self] forecast in
                self?.currentWeatherForecast = forecast
            }
        )
        .store(in: &subscriptions)
    }
}



// MARK: - Private Helpers
private extension CurrentWeatherOnlyExample.ViewModel {

    func setupSubscribers() {
    }


    func makeMeasurementFormatter() -> MeasurementFormatter {
        let formatter = MeasurementFormatter()

        formatter.locale = .current

        return formatter
    }
}
