//
//  CurrentWeatherOnlyExample.swift
//  DemoIOS
//
//  Created by William Starr on 7/1/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI


struct CurrentWeatherOnlyExample {
    @ObservedObject var viewModel: ViewModel
}


// MARK: - View
extension CurrentWeatherOnlyExample: View {

    var body: some View {
        VStack {
            Text("Temperature: \(viewModel.currentTemperatureText)")
            Text("Condition: \(viewModel.conditionText)")
        }
        .onAppear(perform: viewModel.fetchCurrentWeather)
        .navigationBarTitle("Current Weather Forecast")
    }
}


// MARK: - Computeds
extension CurrentWeatherOnlyExample {
}


// MARK: - View Variables
private extension CurrentWeatherOnlyExample {
}


// MARK: - Private Helpers
private extension CurrentWeatherOnlyExample {
}



// MARK: - Preview
struct CurrentWeatherOnlyExample_Previews: PreviewProvider {

    static var previews: some View {
        CurrentWeatherOnlyExample(
            viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
        )
    }
}
