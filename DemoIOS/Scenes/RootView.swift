//
//  RootView.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI


struct RootView {

}


// MARK: - View
extension RootView: View {

    var body: some View {
        NavigationView {
            List {
                NavigationLink(
                    destination: CurrentWeatherOnlyExample(
                        viewModel: .init(weatherLocation: nil)
                    )
                ) {
                    Text("No location provided")
                }
                NavigationLink(
                    destination: CurrentWeatherOnlyExample(
                        viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
                    )
                ) {
                    Text("Fetch Current Weather Only API")
                }
                NavigationLink(
                    destination: CurrentWeatherExample(
                        viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
                    )
                ) {
                    Text("Fetch Current Weather")
                }
                NavigationLink(
                    destination: HourlyWeatherExample(
                        viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
                    )
                ) {
                    Text("Fetch Hourly Weather")
                }
                NavigationLink(
                    destination: DailyWeatherExample(
                        viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
                    )
                ) {
                    Text("Fetch Daily Weather")
                }
            }
            .navigationBarTitle("Timeless Weather")
        }
    }
}


// MARK: - Computeds
extension RootView {
}


// MARK: - View Variables
private extension RootView {
}


// MARK: - Private Helpers
private extension RootView {
}



// MARK: - Preview
struct RootView_Previews: PreviewProvider {

    static var previews: some View {
        RootView()
    }
}
