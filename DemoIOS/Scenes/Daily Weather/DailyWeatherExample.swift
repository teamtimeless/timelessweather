//
//  DailyWeatherExample.swift
//  DemoIOS
//
//  Created by William Starr on 6/29/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI


struct DailyWeatherExample {
    @ObservedObject var viewModel: ViewModel
}


// MARK: - View
extension DailyWeatherExample: View {

    var body: some View {
        List(viewModel.dailyForecast, id: \.date) { daily in
          HStack {
            Text(daily.date).foregroundColor(.red)
            Text(daily.temp)
            Text(daily.condition)
          }
        }
        .onAppear(perform: viewModel.fetchDailyWeather)
        .navigationBarTitle("Daily Weather Forecast")
    }
}


// MARK: - Computeds
extension DailyWeatherExample {
}


// MARK: - View Variables
private extension DailyWeatherExample {
}


// MARK: - Private Helpers
private extension DailyWeatherExample {
}



// MARK: - Preview
struct DailyWeatherExample_Previews: PreviewProvider {

    static var previews: some View {
        DailyWeatherExample(
            viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
        )
    }
}
