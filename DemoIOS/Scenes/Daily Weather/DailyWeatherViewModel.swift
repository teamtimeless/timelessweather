//
//  DailyWeatherViewModel.swift
//  DemoIOS
//
//  Created by William Starr on 6/29/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI
import Combine
import TimelessWeather


extension DailyWeatherExample {
    final class ViewModel: ObservableObject {
        private var subscriptions = Set<AnyCancellable>()

        let weatherLocation: WeatherLocation
        let openWeatherAPIService: OpenWeatherAPIServicing

        lazy var temperatureFormatter: MeasurementFormatter = makeMeasurementFormatter()

        // MARK: - Published Outputs
        @Published var dailyWeatherForecast: Forecast?


        // MARK: - Init
        init(
            weatherLocation: WeatherLocation,
            openWeatherAPIService: OpenWeatherAPIServicing = OpenWeatherAPIService.shared
        ) {
            self.weatherLocation = weatherLocation
            self.openWeatherAPIService = openWeatherAPIService

            self.openWeatherAPIService.apiKey = AppCredentials.OpenWeatherAPI.apiKey

            setupSubscribers()
        }
    }
}


// MARK: - Publishers
extension DailyWeatherExample.ViewModel {

    private var someValuePublisher: AnyPublisher<String, Never> {
        Just("")
            .eraseToAnyPublisher()
    }
}


struct DailyForecast {
    var date: String
    var temp: String
    var condition: String
}


// MARK: - Computeds
extension DailyWeatherExample.ViewModel {
    
    var dailyForecast: [DailyForecast] {
        if dailyWeatherForecast == nil {
            return []
        }
        var _temp: [DailyForecast] = []
        for daily in dailyWeatherForecast!.dailyTimeline {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd HH:mm:ss"
            let date = formatter.string(from: daily.date)
            
            let temp = temperatureFormatter.string(from: daily.temperatureSummary.day)
            let condition = daily.primaryCondition?.rawValue ?? "N/A"
            _temp.append(DailyForecast(date: date, temp: temp, condition: condition))
        }
        
        return _temp
    }

}


// MARK: - Public Methods
extension DailyWeatherExample.ViewModel {

    func fetchDailyWeather() {
        openWeatherAPIService.fetchAllForecast(
            for: weatherLocation
        )
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { completion in

            },
            receiveValue: { [weak self] forecast in
                self?.dailyWeatherForecast = forecast
            }
        )
        .store(in: &subscriptions)
    }
}



// MARK: - Private Helpers
private extension DailyWeatherExample.ViewModel {

    func setupSubscribers() {
    }


    func makeMeasurementFormatter() -> MeasurementFormatter {
        let formatter = MeasurementFormatter()

        formatter.locale = .current

        return formatter
    }
}
