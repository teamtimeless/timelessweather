//
//  HourlyWeatherExample.swift
//  DemoIOS
//
//  Created by William Starr on 6/29/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI


struct HourlyWeatherExample {
    @ObservedObject var viewModel: ViewModel
}


// MARK: - View
extension HourlyWeatherExample: View {

    var body: some View {
        List(viewModel.hourlyForecast, id: \.date) { hourly in
          HStack {
            Text(hourly.date).foregroundColor(.red)
            Text(hourly.temp)
            Text(hourly.condition)
          }
        }
        .onAppear(perform: viewModel.fetchHourlyWeather)
        .navigationBarTitle("Hourly Weather Forecast")
    }
}


// MARK: - Computeds
extension HourlyWeatherExample {
}


// MARK: - View Variables
private extension HourlyWeatherExample {
}


// MARK: - Private Helpers
private extension HourlyWeatherExample {
}



// MARK: - Preview
struct HourlyWeatherExample_Previews: PreviewProvider {

    static var previews: some View {
        HourlyWeatherExample(
            viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
        )
    }
}
