//
//  HourlyWeatherViewModel.swift
//  DemoIOS
//
//  Created by William Starr on 6/29/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI
import Combine
import TimelessWeather


extension HourlyWeatherExample {
    final class ViewModel: ObservableObject {
        private var subscriptions = Set<AnyCancellable>()

        let weatherLocation: WeatherLocation
        let openWeatherAPIService: OpenWeatherAPIServicing

        lazy var temperatureFormatter: MeasurementFormatter = makeMeasurementFormatter()

        // MARK: - Published Outputs
        @Published var hourlyWeatherForecast: Forecast?


        // MARK: - Init
        init(
            weatherLocation: WeatherLocation,
            openWeatherAPIService: OpenWeatherAPIServicing = OpenWeatherAPIService.shared
        ) {
            self.weatherLocation = weatherLocation
            self.openWeatherAPIService = openWeatherAPIService

            self.openWeatherAPIService.apiKey = AppCredentials.OpenWeatherAPI.apiKey

            setupSubscribers()
        }
    }
}


// MARK: - Publishers
extension HourlyWeatherExample.ViewModel {

    private var someValuePublisher: AnyPublisher<String, Never> {
        Just("")
            .eraseToAnyPublisher()
    }
}


struct HourlyForecast {
    var date: String
    var temp: String
    var condition: String
}


// MARK: - Computeds
extension HourlyWeatherExample.ViewModel {
    
    var hourlyForecast: [HourlyForecast] {
        if hourlyWeatherForecast == nil {
            return []
        }
        var _temp: [HourlyForecast] = []
        for hourly in hourlyWeatherForecast!.hourlyTimeline {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd HH:mm:ss"
            let date = formatter.string(from: hourly.date)
            
            let temp = temperatureFormatter.string(from: hourly.temperature)
            let condition = hourly.primaryCondition?.rawValue ?? "N/A"
            _temp.append(HourlyForecast(date: date, temp: temp, condition: condition))
        }
        
        return _temp
    }

}


// MARK: - Public Methods
extension HourlyWeatherExample.ViewModel {

    func fetchHourlyWeather() {
        openWeatherAPIService.fetchAllForecast(
            for: weatherLocation
        )
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { completion in

            },
            receiveValue: { [weak self] forecast in
                self?.hourlyWeatherForecast = forecast
            }
        )
        .store(in: &subscriptions)
    }
}



// MARK: - Private Helpers
private extension HourlyWeatherExample.ViewModel {

    func setupSubscribers() {
    }


    func makeMeasurementFormatter() -> MeasurementFormatter {
        let formatter = MeasurementFormatter()

        formatter.locale = .current

        return formatter
    }
}
