//
//  CurrentWeatherExample+ViewModel.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//


import SwiftUI
import Combine
import TimelessWeather


extension CurrentWeatherExample {
    final class ViewModel: ObservableObject {
        private var subscriptions = Set<AnyCancellable>()

        let weatherLocation: WeatherLocation
        let openWeatherAPIService: OpenWeatherAPIServicing

        lazy var temperatureFormatter: MeasurementFormatter = makeMeasurementFormatter()

        // MARK: - Published Outputs
        @Published var currentWeatherForecast: Forecast?


        // MARK: - Init
        init(
            weatherLocation: WeatherLocation,
            openWeatherAPIService: OpenWeatherAPIServicing = OpenWeatherAPIService.shared
        ) {
            self.weatherLocation = weatherLocation
            self.openWeatherAPIService = openWeatherAPIService

            self.openWeatherAPIService.apiKey = AppCredentials.OpenWeatherAPI.apiKey

            setupSubscribers()
        }
    }
}


// MARK: - Publishers
extension CurrentWeatherExample.ViewModel {

    private var someValuePublisher: AnyPublisher<String, Never> {
        Just("")
            .eraseToAnyPublisher()
    }
}


// MARK: - Computeds
extension CurrentWeatherExample.ViewModel {

    var currentTemperatureDetails: CurrentWeatherDataItem? {
        currentWeatherForecast?.currentWeather
    }

    var currentTemperatureFahrenheit: Double? {
        currentTemperatureDetails?.temperature.converted(to: .fahrenheit).value
    }

    var currentTemperatureCelsius: Double? {
        currentTemperatureDetails?.temperature.converted(to: .celsius).value
    }

    var currentTemperatureText: String {
        guard let currentTemperature = currentTemperatureDetails?.temperature else { return "N/A" }

        return temperatureFormatter.string(from: currentTemperature)
    }

    var conditionText: String {
        currentWeatherForecast?.primaryCondition?.rawValue ?? "N/A"
    }
}


// MARK: - Public Methods
extension CurrentWeatherExample.ViewModel {

    func fetchCurrentWeather() {
        openWeatherAPIService.fetchAllForecast(
            for: weatherLocation
        )
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { completion in

            },
            receiveValue: { [weak self] forecast in
                self?.currentWeatherForecast = forecast
            }
        )
        .store(in: &subscriptions)
    }
}



// MARK: - Private Helpers
private extension CurrentWeatherExample.ViewModel {

    func setupSubscribers() {
    }


    func makeMeasurementFormatter() -> MeasurementFormatter {
        let formatter = MeasurementFormatter()

        formatter.locale = .current

        return formatter
    }
}
