//
//  CurrentWeatherExample.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import SwiftUI


struct CurrentWeatherExample {
    @ObservedObject var viewModel: ViewModel
}


// MARK: - View
extension CurrentWeatherExample: View {

    var body: some View {
        VStack {
            Text("Temperature: \(viewModel.currentTemperatureText)")
            Text("Condition: \(viewModel.conditionText)")
        }
        .onAppear(perform: viewModel.fetchCurrentWeather)
        .navigationBarTitle("Current Weather Forecast")
    }
}


// MARK: - Computeds
extension CurrentWeatherExample {
}


// MARK: - View Variables
private extension CurrentWeatherExample {
}


// MARK: - Private Helpers
private extension CurrentWeatherExample {
}



// MARK: - Preview
struct CurrentWeatherExample_Previews: PreviewProvider {

    static var previews: some View {
        CurrentWeatherExample(
            viewModel: .init(weatherLocation: PreviewData.WeatherLocations.chicago)
        )
    }
}
