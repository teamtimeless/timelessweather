//
//  TimelessWeather.h
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TimelessWeather.
FOUNDATION_EXPORT double TimelessWeatherVersionNumber;

//! Project version string for TimelessWeather.
FOUNDATION_EXPORT const unsigned char TimelessWeatherVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TimelessWeather/PublicHeader.h>


