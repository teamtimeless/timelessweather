//
//  Units.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


/// Base Unit: `Kelvin`.
public typealias Temperature = Measurement<UnitTemperature>


/// Atmospheric pressure on the sea level.
///
/// Base Unit: Hectopascal (hPa)
public typealias AtmosphericPressure = Measurement<UnitPressure>


/// Base Unit: Meters
public typealias Visibility = Measurement<UnitLength>


/// Base Unit: Meters per second
public typealias WindSpeed = Measurement<UnitSpeed>


/// Base Unit: [Degrees (meteorological)](https://en.wikipedia.org/wiki/Wind_direction)
public typealias WindDirection = Measurement<UnitAngle>
