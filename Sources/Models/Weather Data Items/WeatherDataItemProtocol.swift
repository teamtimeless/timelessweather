//
//  WeatherDataItemProtocol.swift
//  TimelessWeather
//
// Created by Brian Sipple on 5/5/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation



public protocol WeatherDataItemProtocol {
    var date: Date { get }
    var pressure: AtmosphericPressure { get }

    /// Humidity percentage [0, 100]
    var humidityPercentage: Double { get }

    /// Cloudiness percentage [0, 100]
    var cloudinessPercentage: Double { get }

    /// Average visibility
    var visibility: Visibility? { get }

    var windSpeed: WindSpeed { get }
    var windDirection: WindDirection { get }

    var conditionData: [WeatherConditionDataItem] { get }
}
