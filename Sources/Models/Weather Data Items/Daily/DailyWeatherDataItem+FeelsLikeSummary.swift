//
//  DailyWeatherDataItem+FeelsLikeSummary.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


extension DailyWeatherDataItem {

    public struct FeelsLikeSummary {
        public let day: Temperature
        public let night: Temperature
        public let morning: Temperature
        public let evening: Temperature
    }
}



// MARK: - Decodable
extension DailyWeatherDataItem.FeelsLikeSummary: Decodable {

    enum CodingKeys: String, CodingKey {
        case day = "day"
        case night = "night"
        case morning = "morn"
        case evening = "eve"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let dayValue = try container.decode(Double.self, forKey: .day)
        day = Temperature(value: dayValue, unit: .kelvin)
        
        let nightValue = try container.decode(Double.self, forKey: .night)
        night = Temperature(value: nightValue, unit: .kelvin)
        
        let morningValue = try container.decode(Double.self, forKey: .morning)
        morning = Temperature(value: morningValue, unit: .kelvin)
        
        let eveningValue = try container.decode(Double.self, forKey: .evening)
        evening = Temperature(value: eveningValue, unit: .kelvin)
    }
}
