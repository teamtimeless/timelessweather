//
//  DailyWeatherDataItem+TemperatureSummary.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


extension DailyWeatherDataItem {

    public struct TemperatureSummary {
        public let high: Temperature
        public let low: Temperature
        public let day: Temperature
        public let night: Temperature
        public let morning: Temperature
        public let evening: Temperature
    }
}


// MARK: - Decodable
extension DailyWeatherDataItem.TemperatureSummary: Decodable {

    enum CodingKeys: String, CodingKey {
        case high = "max"
        case low = "min"
        case day = "day"
        case night = "night"
        case morning = "morn"
        case evening = "eve"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let highValue = try container.decode(Double.self, forKey: .high)
        high = Temperature(value: highValue, unit: .kelvin)
        
        let lowValue = try container.decode(Double.self, forKey: .low)
        low = Temperature(value: lowValue, unit: .kelvin)
        
        let dayValue = try container.decode(Double.self, forKey: .day)
        day = Temperature(value: dayValue, unit: .kelvin)
        
        let nightValue = try container.decode(Double.self, forKey: .night)
        night = Temperature(value: nightValue, unit: .kelvin)
        
        let morningValue = try container.decode(Double.self, forKey: .morning)
        morning = Temperature(value: morningValue, unit: .kelvin)
        
        let eveningValue = try container.decode(Double.self, forKey: .evening)
        evening = Temperature(value: eveningValue, unit: .kelvin)
    }
}
