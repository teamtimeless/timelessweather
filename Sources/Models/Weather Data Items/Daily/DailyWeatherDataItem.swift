//
//  DailyWeatherDataItem.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation



public struct DailyWeatherDataItem: WeatherDataItemProtocol {
    public let date: Date

    public let sunrise: Date
    public let sunset: Date


    /// Summary of daily temperatures. This includes temperature values for
    /// `high`, `low`, `day`, `night`, `morning` and `evening`
    ///
    /// Base Unit: `Kelvin`.
    public let temperatureSummary: TemperatureSummary


    /// Summary of daily feels like,This includes temperature values for
    /// `day`, `night`, `morning` and `evening`
    /// Adjusted after accounting for the human perception of weather.
    ///
    /// Base Unit: `Kelvin`.
    public let feelsLikeSummary: FeelsLikeSummary


    public let pressure: AtmosphericPressure
    public let humidityPercentage: Double

    /// [Usage](https://en.wikipedia.org/wiki/Ultraviolet_index#Index_usage)
    public let uvIndex: Double

    public let cloudinessPercentage: Double
    public let visibility: Visibility?

    public let windSpeed: WindSpeed
    public let windDirection: WindDirection
    
    public let rain: Double?
    public let snow: Double?

    public let conditionData: [WeatherConditionDataItem]
    public let primaryCondition: WeatherCondition?
}


// MARK: - Identifiable
extension DailyWeatherDataItem: Identifiable {
    public var id: Date { date }
}


// MARK: - Decodable
extension DailyWeatherDataItem: Decodable {

    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case sunrise = "sunrise"
        case sunset = "sunset"
        case temperatureSummary = "temp"
        case feelsLikeSummary = "feels_like"
        case pressure = "pressure"
        case humidityPercentage = "humidity"
        case uvIndex = "uvi"
        case cloudinessPercentage = "clouds"
        case visibility = "visibility"
        case windSpeed = "wind_speed"
        case windDirection = "wind_deg"
        case rain = "rain"
        case snow = "snow"
        case conditionData = "weather"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        date = try container.decode(Date.self, forKey: .date)
        sunrise = try container.decode(Date.self, forKey: .sunrise)
        sunset = try container.decode(Date.self, forKey: .sunset)
        
        
        temperatureSummary = try container.decode(TemperatureSummary.self, forKey: .temperatureSummary)

        feelsLikeSummary = try container.decode(FeelsLikeSummary.self, forKey: .feelsLikeSummary)

        let pressureValue = try container.decode(Double.self, forKey: .pressure)
        pressure = AtmosphericPressure(value: pressureValue, unit: .hectopascals)

        humidityPercentage = try container.decode(Double.self, forKey: .humidityPercentage)

        cloudinessPercentage = try container.decode(Double.self, forKey: .cloudinessPercentage)
        
        uvIndex = try container.decode(Double.self, forKey: .uvIndex)

        if let visibilityValue = try container.decodeIfPresent(Double.self, forKey: .visibility) {
            visibility = Visibility(value: visibilityValue, unit: .meters)
        } else {
            visibility = nil
        }

        let windSpeedValue = try container.decode(Double.self, forKey: .windSpeed)
        windSpeed = WindSpeed(value: windSpeedValue, unit: .metersPerSecond)

        let windDirectionValue = try container.decode(Double.self, forKey: .windDirection)
        windDirection = WindDirection(value: windDirectionValue, unit: .degrees)
        
        rain = try? container.decodeIfPresent(Double.self, forKey: .rain) ?? nil
        snow = try? container.decodeIfPresent(Double.self, forKey: .snow) ?? nil

        conditionData = try container.decode([WeatherConditionDataItem].self, forKey: .conditionData)
        primaryCondition = conditionData.first?.condition
    }
}
