//
//  CurrentWeatherDataItem.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation



public struct CurrentWeatherDataItem: WeatherDataItemProtocol {
    public let date: Date

    public let sunrise: Date
    public let sunset: Date

    public let temperature: Temperature

    /// The current temperature, adjusted after accounting for the human perception of weather.
    public let feelsLike: Temperature

    public let pressure: AtmosphericPressure
    public let humidityPercentage: Double

    /// [Usage](https://en.wikipedia.org/wiki/Ultraviolet_index#Index_usage)
    public let uvIndex: Double

    public let cloudinessPercentage: Double
    public let visibility: Visibility?

    public let windSpeed: WindSpeed
    public let windDirection: WindDirection
    
    public let rain: Precipitation?
    public let snow: Precipitation?

    public let conditionData: [WeatherConditionDataItem]
}


// MARK: - Identifiable
extension CurrentWeatherDataItem: Identifiable {
    public var id: Date { date }
}


// MARK: - Decodable
extension CurrentWeatherDataItem: Decodable {

    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case sunrise = "sunrise"
        case sunset = "sunset"
        case temperature = "temp"
        case feelsLike = "feels_like"
        case pressure = "pressure"
        case humidityPercentage = "humidity"
        case uvIndex = "uvi"
        case cloudinessPercentage = "clouds"
        case visibility = "visibility"
        case windSpeed = "wind_speed"
        case windDirection = "wind_deg"
        case rain = "rain"
        case snow = "snow"
        case conditionData = "weather"
    }


    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        date = try container.decode(Date.self, forKey: .date)
        sunrise = try container.decode(Date.self, forKey: .sunrise)
        sunset = try container.decode(Date.self, forKey: .sunset)

        let temperatureValue = try container.decode(Double.self, forKey: .temperature)
        temperature = Temperature(value: temperatureValue, unit: .kelvin)

        let feelsLikeValue = try container.decode(Double.self, forKey: .feelsLike)
        feelsLike = Temperature(value: feelsLikeValue, unit: .kelvin)

        let pressureValue = try container.decode(Double.self, forKey: .pressure)
        pressure = AtmosphericPressure(value: pressureValue, unit: .hectopascals)

        humidityPercentage = try container.decode(Double.self, forKey: .humidityPercentage)
        uvIndex = try container.decode(Double.self, forKey: .uvIndex)

        cloudinessPercentage = try container.decode(Double.self, forKey: .cloudinessPercentage)

        if let visibilityValue = try container.decodeIfPresent(Double.self, forKey: .visibility) {
            visibility = Visibility(value: visibilityValue, unit: .meters)
        } else {
            visibility = nil
        }

        let windSpeedValue = try container.decode(Double.self, forKey: .windSpeed)
        windSpeed = WindSpeed(value: windSpeedValue, unit: .metersPerSecond)

        let windDirectionValue = try container.decode(Double.self, forKey: .windDirection)
        windDirection = WindDirection(value: windDirectionValue, unit: .degrees)

        rain = try? container.decodeIfPresent(Precipitation.self, forKey: .rain) ?? nil
        snow = try? container.decodeIfPresent(Precipitation.self, forKey: .snow) ?? nil
        
        conditionData = try container.decode([WeatherConditionDataItem].self, forKey: .conditionData)
    }
}

