//
//  CurrentWeatherForecast.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct CurrentWeatherForecast {
    public let latitude: Double
    public let longitude: Double
    public let timezone: String
    public let details: CurrentWeatherDataItem
}


// MARK: - Identifiable
extension CurrentWeatherForecast: Identifiable {
    public var id: UUID { .init() }
}


// MARK: - Decodable
extension CurrentWeatherForecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
        case timezone = "timezone"
        case details = "current"
    }
}


// MARK: - Computeds
extension CurrentWeatherForecast {
    public var primaryCondition: WeatherCondition? {
        details.conditionData.first?.condition
    }
}
