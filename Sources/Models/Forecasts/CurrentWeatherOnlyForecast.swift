//
//  CurrentWeatherOnlyForecast.swift
//  TimelessWeather
//
//  Created by William Starr on 7/1/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct CurrentWeatherOnlyForecast {
    public let latitude: Double
    public let longitude: Double
    public let timezoneOffset: Int
    private let coordPrivate: Coord
    private let mainPrivate: Main
    private let sysPrivate: Sys
    private let cloudsPrivate: Clouds
    private let windPrivate: Wind
    
    public let date: Date

    public let sunrise: Date
    public let sunset: Date

    public let temperature: Temperature

    /// The current temperature, adjusted after accounting for the human perception of weather.
    public let feelsLike: Temperature

    public let pressure: AtmosphericPressure
    public let humidityPercentage: Double

    public let cloudinessPercentage: Double
    public let visibility: Visibility?

    public let windSpeed: WindSpeed
    public let windDirection: WindDirection
    
    public let rain: Precipitation?
    public let snow: Precipitation?

    public let conditionData: [WeatherConditionDataItem]
}


// MARK: - Identifiable
extension CurrentWeatherOnlyForecast: Identifiable {
    public var id: UUID { .init() }
}


// MARK: - Decodable
extension CurrentWeatherOnlyForecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
        case timezoneOffset = "timezone"
        case details = "current"
        case coordPrivate = "coord"
        case mainPrivate = "main"
        case sysPrivate = "sys"
        case cloudsPrivate = "clouds"
        case windPrivate = "wind"
        
        case date = "dt"
        case sunrise = "sunrise"
        case sunset = "sunset"
        case temperature = "temp"
        case feelsLike = "feels_like"
        case pressure = "pressure"
        case humidityPercentage = "humidity"
        case visibility = "visibility"
        case windSpeed = "wind_speed"
        case windDirection = "wind_deg"
        case rain = "rain"
        case snow = "snow"
        case conditionData = "weather"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        coordPrivate = try container.decode(Coord.self, forKey: .coordPrivate)
        mainPrivate = try container.decode(Main.self, forKey: .mainPrivate)
        sysPrivate = try container.decode(Sys.self, forKey: .sysPrivate)
        
        timezoneOffset = try container.decode(Int.self, forKey: .timezoneOffset)
        latitude = coordPrivate.lat
        longitude = coordPrivate.lon

        date = try container.decode(Date.self, forKey: .date)
        sunrise = sysPrivate.sunrise
        sunset = sysPrivate.sunset

        let temperatureValue = mainPrivate.temp
        temperature = Temperature(value: temperatureValue, unit: .kelvin)

        let feelsLikeValue = mainPrivate.feelsLike
        feelsLike = Temperature(value: feelsLikeValue, unit: .kelvin)

        let pressureValue = mainPrivate.pressure
        pressure = AtmosphericPressure(value: pressureValue, unit: .hectopascals)

        humidityPercentage = mainPrivate.humidity

        cloudsPrivate = try container.decode(Clouds.self, forKey: .cloudsPrivate)
        cloudinessPercentage = cloudsPrivate.all

        if let visibilityValue = try container.decodeIfPresent(Double.self, forKey: .visibility) {
            visibility = Visibility(value: visibilityValue, unit: .meters)
        } else {
            visibility = nil
        }
        
        windPrivate = try container.decode(Wind.self, forKey: .windPrivate)
        let windSpeedValue = windPrivate.speed
        windSpeed = WindSpeed(value: windSpeedValue, unit: .metersPerSecond)

        let windDirectionValue = windPrivate.deg
        windDirection = WindDirection(value: windDirectionValue, unit: .degrees)

        rain = try? container.decodeIfPresent(Precipitation.self, forKey: .rain) ?? nil
        snow = try? container.decodeIfPresent(Precipitation.self, forKey: .snow) ?? nil
        
        conditionData = try container.decode([WeatherConditionDataItem].self, forKey: .conditionData)
    }
}


// MARK: - Computeds
extension CurrentWeatherOnlyForecast {
    public var primaryCondition: WeatherCondition? {
        conditionData.first?.condition
    }
}


// MARK: - Coord
public struct Coord {
    internal let lon: Double
    internal let lat: Double
}


// MARK: - Coord Decodable
extension Coord: Decodable {

    enum CodingKeys: String, CodingKey {
        case lon = "lon"
        case lat = "lat"
    }
}



// MARK: - Main
public struct Main {
    internal let temp: Double
    internal let feelsLike: Double
    internal let pressure: Double
    internal let humidity: Double
    internal let tempMin: Double
    internal let tempMax: Double
}


// MARK: - Main Decodable
extension Main: Decodable {

    enum CodingKeys: String, CodingKey {
        case temp = "temp"
        case feelsLike = "feels_like"
        case pressure = "pressure"
        case humidity = "humidity"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
}


// MARK: - Sys
public struct Sys {
    internal let country: String?
    internal let sunrise: Date
    internal let sunset: Date
}


// MARK: - Sys Decodable
extension Sys: Decodable {

    enum CodingKeys: String, CodingKey {
        case country = "country"
        case sunrise = "sunrise"
        case sunset = "sunset"
    }
}


// Mark: - Clouds
internal struct Clouds {
    internal let all: Double
}

// MARK: - Clouds Decodable
extension Clouds: Decodable {

    enum CodingKeys: String, CodingKey {
        case all = "all"
    }
}


// Mark: - Wind
public struct Wind {
    internal let speed: Double
    internal let deg: Double
}

// MARK: - Wind Decodable
extension Wind: Decodable {

    enum CodingKeys: String, CodingKey {
        case speed = "speed"
        case deg = "deg"
    }
}
