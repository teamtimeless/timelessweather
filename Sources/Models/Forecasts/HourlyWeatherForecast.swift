//
//  HourlyWeatherForecast.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct HourlyWeatherForecast {
    public let latitude: Double
    public let longitude: Double
    public let timezone: String
    public let timeline: [HourlyWeatherDataItem]
}


// MARK: - Identifiable
extension HourlyWeatherForecast: Identifiable {
    public var id: UUID { .init() }
}


// MARK: - Decodable
extension HourlyWeatherForecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
        case timezone = "timezone"
        case timeline = "hourly"
    }
}
