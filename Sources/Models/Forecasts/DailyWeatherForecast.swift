//
//  DailyWeatherForecast.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct DailyWeatherForecast {
    public let latitude: Double
    public let longitude: Double
    public let timezone: String
    public let timeline: [DailyWeatherDataItem]
}



// MARK: - Identifiable
extension DailyWeatherForecast: Identifiable {
    public var id: UUID { .init() }
}


// MARK: - Decodable
extension DailyWeatherForecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
        case timezone = "timezone"
        case timeline = "daily"
    }
}


// MARK: - Computeds
extension DailyWeatherForecast {
    public var timelineMap: [Date: DailyWeatherDataItem] {
        return timeline.reduce(into: [Date: DailyWeatherDataItem]()) {
            $0[$1.date] = $1
        }
    }
}
