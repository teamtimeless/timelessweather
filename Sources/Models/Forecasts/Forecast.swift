//
//  Forecast.swift
//  TimelessWeather
//
//  Created by William Starr on 7/1/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct Forecast {
    public let latitude: Double
    public let longitude: Double
    public let timezone: String
    public let currentWeather: CurrentWeatherDataItem
    public let hourlyTimeline: [HourlyWeatherDataItem]
    public let dailyTimeline: [DailyWeatherDataItem]
}


// MARK: - Identifiable
extension Forecast: Identifiable {
    public var id: UUID { .init() }
}


// MARK: - Decodable
extension Forecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
        case timezone = "timezone"
        case currentWeather = "current"
        case hourlyTimeline = "hourly"
        case dailyTimeline = "daily"
    }
}


// MARK: - Computeds
extension Forecast {
    public var primaryCondition: WeatherCondition? {
        currentWeather.conditionData.first?.condition
    }
    
    public var dailyTimelineMap: [Date: DailyWeatherDataItem] {
        return dailyTimeline.reduce(into: [Date: DailyWeatherDataItem]()) {
            $0[$1.date] = $1
        }
    }
    
    public var hourlyTimelineMap: [Date: HourlyWeatherDataItem] {
        return hourlyTimeline.reduce(into: [Date: HourlyWeatherDataItem]()) {
            $0[$1.date] = $1
        }
    }
}
