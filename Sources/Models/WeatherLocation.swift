//
//  WeatherLocation.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct WeatherLocation {
    public let latitude: Double
    public let longitude: Double

    
    public init(
        latitude: Double,
        longitude: Double
    ) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

