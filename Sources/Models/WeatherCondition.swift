//
//  WeatherCondition.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public enum WeatherCondition: String {
    case clearDay
    case clearNight

    case partlyCloudyDay
    case partlyCloudyNight

    case cloudyDay
    case cloudyNight

    case dustDay
    case dustNight

    case fogDay
    case fogNight

    // Rain
    case rainDay
    case rainNight

    /// A heavier version of rain (?)
    case showerDay
    case showerNight

    // Snow
    case snowDay
    case snowNight

    // Thunderstorm
    case thunderstormDay
    case thunderstormNight

    // Tornado
    case tornadoDay
    case tornadoNight
}


extension WeatherCondition: CaseIterable {}
extension WeatherCondition: Identifiable {
    public var id: String { rawValue }
}
