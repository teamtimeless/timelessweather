//
//  Precipitation.swift
//  TimelessWeather
//
//  Created by William Starr on 6/29/20.
//  Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct Precipitation {
    public let lastHour: Double
}


// MARK: - Decodable
extension Precipitation: Decodable {

    enum CodingKeys: String, CodingKey {
        case lastHour = "1h"
    }
}
