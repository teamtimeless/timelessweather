//
//  WeatherConditionDataItem.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public struct WeatherConditionDataItem {
    public let code: Int
    public let categoryName: String
    public let conditionDescription: String

    private let openWeatherIconName: String
}


// MARK: - Identifiable
extension WeatherConditionDataItem: Identifiable {
    public var id: Int { code }
}


// MARK: - Decodable
extension WeatherConditionDataItem: Decodable {

    enum CodingKeys: String, CodingKey {
        case code = "id"
        case categoryName = "main"
        case conditionDescription = "description"
        case openWeatherIconName = "icon"
    }
}


// MARK: - Computeds
extension WeatherConditionDataItem {

    /// Timeless weather conditions as derived from `OpenWeatherMap`'s
    /// [weather condition codes](https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2)
    public var condition: WeatherCondition? {
        switch openWeatherIconName {
            // Sunny/Clear
        case "01d":
            return .clearDay
        case "01n":
            return .clearNight
            // Cloudy/PartlyCloudy
        case "02d":
            return .partlyCloudyDay
        case "02n":
            return .partlyCloudyNight
        case "03d", "04d":
            return .cloudyDay
        case "03n", "04n":
            return .cloudyNight
            // Rain / Shower
        case "09d":
            return .showerDay
        case "09n":
            return .showerNight
        case "10d":
            return .rainDay
        case "10n":
            return .rainNight
            // ThunderStorm
        case "11d":
            return .thunderstormDay
        case "11n":
            return .thunderstormNight
            // Snow
        case "13d":
            return .snowDay
        case "13n":
            return .snowNight
            // Mist
        case "50d":
            return code == 761 ? .dustDay : code == 781 ? .tornadoDay : .fogDay
        case "50n":
            return code == 761 ? .dustNight : code == 781 ? .tornadoNight : .fogNight
        default:
            return nil
        }
    }
}
