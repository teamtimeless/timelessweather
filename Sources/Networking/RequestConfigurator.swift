//
//  RequestConfigurator.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/19/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


typealias HTTPHeaders = [String: String]


enum RequestConfigurator {
    
    static func configure(
        headers: HTTPHeaders,
        for request: inout URLRequest
    ) {
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
