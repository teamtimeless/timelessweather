//
//  HTTPHeaderField.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/19/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


enum HTTPHeaderField {
    static let accept = "Accept"
    static let contentType = "Content-Type"
    static let cacheControl = "Cache-Control"
}
