//
//  Endpoint+OpenWeatherAPI.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


extension Endpoint {

    enum OpenWeatherAPI {
        static let scheme = "https"
        static let host = "api.openweathermap.org"
        static let apiKeyEnvironmentKey = "OPEN_WEATHER_API_KEY"
        
        static let onePath = "/data/2.5/onecall"
        static let currentPath = "/data/2.5/weather"

        enum QueryItemName {
            static let latitude = "lat"
            static let longitude = "lon"
            static let apiKey = "appid"
        }


        static func oneCall(
            for location: WeatherLocation,
            using apiKey: String
        ) -> Endpoint {
            Endpoint(
                scheme: scheme,
                host: host,
                path: onePath,
                queryItems: [
                    .init(name: QueryItemName.latitude, value: "\(location.latitude)"),
                    .init(name: QueryItemName.longitude, value: "\(location.longitude)"),
                    .init(name: QueryItemName.apiKey, value: apiKey),
                ]
            )
        }
        
        static func currentWeather(
            for location: WeatherLocation,
            using apiKey: String
        ) -> Endpoint {
            Endpoint(
                scheme: scheme,
                host: host,
                path: currentPath,
                queryItems: [
                    .init(name: QueryItemName.latitude, value: "\(location.latitude)"),
                    .init(name: QueryItemName.longitude, value: "\(location.longitude)"),
                    .init(name: QueryItemName.apiKey, value: apiKey),
                ]
            )
        }
    }
}

internal enum ApiEndpoints {
    case oneCall
    case currentWeather
}
