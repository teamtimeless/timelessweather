//
//  Endpoint.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


struct Endpoint {
    var scheme: String
    var host: String
    var path: String
    var queryItems: [URLQueryItem]?


    // MARK: - Init
    init(
        scheme: String = "https",
        host: String,
        path: String,
        queryItems: [URLQueryItem]? = nil
    ) {
        self.scheme = scheme
        self.host = host
        self.path = path
        self.queryItems = queryItems
    }
}



// MARK: - Computeds
extension Endpoint {

    var urlComponents: URLComponents {
        var urlComponents = URLComponents()

        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = queryItems

        return urlComponents
    }

    var url: URL? { urlComponents.url }
}
