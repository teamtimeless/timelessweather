//
//  OpenWeatherAPIServiceError+ParseFrom.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/18/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation



extension OpenWeatherAPIServiceError {

    static func parseFrom(
        _ data: Data,
        and response: HTTPURLResponse
    ) -> Result<Void, OpenWeatherAPIServiceError>  {
        switch response.statusCode {
        case 200 ..< 300:
            return .success(())
        case 400:
            return .failure(.badRequest(data, response))
        case 401:
            return .failure(.unauthorizedRequest)
        case 404:
            return .failure(.notFound)
        case 429:
            return .failure(.tooManyRequests(data, response))
        case 500 ..< 600:
            return .failure(.severSideFailure(data, response))
        default:
            return .failure(.unsuccessfulRequest(data, response))
        }
    }
}
