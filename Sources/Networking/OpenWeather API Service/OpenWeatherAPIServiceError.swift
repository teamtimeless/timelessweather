//
//  OpenWeatherAPIServiceError.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public enum OpenWeatherAPIServiceError: Error {
    case missingLocation
    case missingAPIKey
    case apiURLCreation
    case missingURL

    /// 📝 NOTE: Despite errors having an `errorDescription`, It might still be useful to
    /// inspect the full `EncodingError` instance returned as the error case's associated value.
    case dataDecodingFailed(DecodingError)

    case missingResponse
    case responseMissingData
    case requestFailed(error: Error)
    case unauthorizedRequest
    case badRequest(_ data: Data, _ response: HTTPURLResponse)
    case tooManyRequests(_ data: Data, _ response: HTTPURLResponse)
    case severSideFailure(_ data: Data, _ response: HTTPURLResponse)
    case notFound
    case unsuccessfulRequest(_ data: Data, _ response: HTTPURLResponse)
    case generic(error: Error)
}


// MARK: - Error Descriptions
extension OpenWeatherAPIServiceError: LocalizedError {

    public var errorDescription: String? {
        switch self {
        case .missingLocation:
            return "No location provided."
        case .missingResponse:
            return "No response was returned for the request."
        case .dataDecodingFailed(let decodingError):
            return "Error while attempting to decode data: \(decodingError.errorDescription ?? decodingError.localizedDescription)."
        case .requestFailed(let error):
            return "The request failed with an error: \"\(error.localizedDescription)\""
        case .generic(let error):
            return error.localizedDescription
        case .missingURL:
            return "No URL was found in the request."
        case .responseMissingData:
            return "No data was returned in the response."
        case .unauthorizedRequest:
            return "The request was made without authorization."
        case .notFound:
            return "The requested resource could not be found."
        case .badRequest:
            return "The request was considered to be a bad request"
        case .tooManyRequests:
            return "Too many requests have been sent within a given amount of time."
        case .severSideFailure(_, let response):
            return "The request failed due to a server-side error. (Status Code: \(response.statusCode))."
        case .unsuccessfulRequest(_, let response):
            return "The request was unsuccessful. (Status Code: \(response.statusCode))."
        case .apiURLCreation:
            return "A URL could not be created for the request."
        case .missingAPIKey:
            return """
            No api key could be found for the request.
            Please create an environment variable in your project named \
            \"\(Endpoint.OpenWeatherAPI.apiKeyEnvironmentKey)\" and set it to
            the value of your OpenWeather API Key (https://home.openweathermap.org/api_keys).
            """
        }
    }
}


// MARK: - Identifiable
extension OpenWeatherAPIServiceError: Identifiable {
    public var id: String? { errorDescription }
}
