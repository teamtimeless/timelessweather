//
//  OpenWeatherAPIServicing.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation
import Combine


public protocol OpenWeatherAPIServicing: class {
    var taskPublisher: SessionDataTaskPublishing { get }
    var subscriptionQueue: DispatchQueue { get }
    var apiKey: String? { get set }


    func fetchCurrentWeatherOnlyForecast(
        for location: WeatherLocation?,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<CurrentWeatherOnlyForecast, OpenWeatherAPIServiceError>


    func fetchAllForecast(
        for location: WeatherLocation?,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<Forecast, OpenWeatherAPIServiceError>


    @available(*, deprecated, message: "Please use fetchCurrentWeatherOnlyForecast.")
    func fetchCurrentWeatherForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<CurrentWeatherForecast, OpenWeatherAPIServiceError>


    @available(*, deprecated, message: "Please use fetchAllForecast.")
    func fetchHourlyForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<HourlyWeatherForecast, OpenWeatherAPIServiceError>


    @available(*, deprecated, message: "Please use fetchAllForecast.")
    func fetchDailyForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<DailyWeatherForecast, OpenWeatherAPIServiceError>
}


// MARK: - Default Implementation
extension OpenWeatherAPIServicing {


    public func fetchCurrentWeatherOnlyForecast(
        for location: WeatherLocation?,
        using decoder: JSONDecoder = OpenWeatherAPIService.defaultDecoder,
        maxRetries: Int = 0
    ) -> AnyPublisher<CurrentWeatherOnlyForecast, OpenWeatherAPIServiceError> {
        fetchCurrentForecast(at: location, using: decoder, maxRetries: maxRetries)
    }


    public func fetchCurrentWeatherForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder = OpenWeatherAPIService.defaultDecoder,
        maxRetries: Int = 0
    ) -> AnyPublisher<CurrentWeatherForecast, OpenWeatherAPIServiceError> {
        fetchForecast(at: location, using: decoder, maxRetries: maxRetries)
    }


    public func fetchAllForecast(
        for location: WeatherLocation?,
        using decoder: JSONDecoder = OpenWeatherAPIService.defaultDecoder,
        maxRetries: Int = 0
    ) -> AnyPublisher<Forecast, OpenWeatherAPIServiceError> {
        fetchForecast(at: location, using: decoder, maxRetries: maxRetries)
    }


    public func fetchHourlyForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder = OpenWeatherAPIService.defaultDecoder,
        maxRetries: Int = 0
    ) -> AnyPublisher<HourlyWeatherForecast, OpenWeatherAPIServiceError> {
        fetchForecast(at: location, using: decoder, maxRetries: maxRetries)
    }


    public func fetchDailyForecast(
        for location: WeatherLocation,
        using decoder: JSONDecoder = OpenWeatherAPIService.defaultDecoder,
        maxRetries: Int = 0
    ) -> AnyPublisher<DailyWeatherForecast, OpenWeatherAPIServiceError> {
        fetchForecast(at: location, using: decoder, maxRetries: maxRetries)
    }
}


// MARK: - Helpers
extension OpenWeatherAPIServicing {

    internal func oneCallUrl(for location: WeatherLocation, using apiKey: String) -> URL? {
        Endpoint.OpenWeatherAPI.oneCall(for: location, using: apiKey).url
    }
    
    internal func currentWeatherUrl(for location: WeatherLocation, using apiKey: String) -> URL? {
        Endpoint.OpenWeatherAPI.currentWeather(for: location, using: apiKey).url
    }

    /// Uses the `session` to perform a data task for a `URLRequest`,
    /// then attempts to handle request errors and data decoding.
    ///
    /// - Parameters:
    ///   - receptionQueue: A queue to be used for parsing the response -- ideally off
    ///     the main thread.
    private func execute(
        _ taskPublisher: URLSession.DataTaskPublisher,
        maxRetries allowedRetries: Int = 0
    ) -> AnyPublisher<Data, OpenWeatherAPIServiceError> {
        taskPublisher
            .subscribe(on: subscriptionQueue)
            .retry(allowedRetries)
            .mapError( { OpenWeatherAPIServiceError.requestFailed(error: $0) })
            .tryMap { (data: Data, response: URLResponse) in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw OpenWeatherAPIServiceError.missingResponse
                }

                if case .failure(let foundError) = OpenWeatherAPIServiceError.parseFrom(data, and: httpResponse) {
                    throw foundError
                }

                return data
            }
            .mapError( { $0 as? OpenWeatherAPIServiceError ?? .generic(error: $0) })
            .eraseToAnyPublisher()
    }


    internal func makeRequestPublisher(for location: WeatherLocation, using apiKey: String, api: ApiEndpoints) -> URLSession.DataTaskPublisher? {
        var url: URL?
        if api == .oneCall {
            guard let _url = oneCallUrl(for: location, using: apiKey) else { return nil }
            url = _url
        } else if api == .currentWeather {
            guard let _url = currentWeatherUrl(for: location, using: apiKey) else { return nil }
            url = _url
        }

        var request = URLRequest(url: url!)

        RequestConfigurator.configure(
            headers: [
                HTTPHeaderField.contentType: "application/json",
                HTTPHeaderField.cacheControl: "no-cache",
            ],
            for: &request
        )

        return taskPublisher.dataTaskPublisher(for: request)
    }
    
    
    internal func fetchCurrentForecast<Forecast: Decodable>(
        at location: WeatherLocation?,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<Forecast, OpenWeatherAPIServiceError> {
        guard let location = location else {
            return Fail(error: .missingLocation).eraseToAnyPublisher()
        }
        
        guard let apiKey = apiKey else {
            return Fail(error: .missingAPIKey).eraseToAnyPublisher()
        }

        guard let taskPublisher = makeRequestPublisher(for: location, using: apiKey, api: .currentWeather) else {
            return Fail(error: .apiURLCreation).eraseToAnyPublisher()
        }

        return execute(taskPublisher, maxRetries: maxRetries)
            .decode(type: Forecast.self, decoder: decoder)
            .mapError { error in
                switch error {
                case let serviceError as OpenWeatherAPIServiceError:
                    return serviceError
                case let decodingError as DecodingError:
                    print(decodingError)
                    return OpenWeatherAPIServiceError.dataDecodingFailed(decodingError)
                default:
                    return OpenWeatherAPIServiceError.generic(error: error)
                }
            }
            .eraseToAnyPublisher()
    }


    internal func fetchForecast<Forecast: Decodable>(
        at location: WeatherLocation?,
        using decoder: JSONDecoder,
        maxRetries: Int
    ) -> AnyPublisher<Forecast, OpenWeatherAPIServiceError> {
        guard let location = location else {
            return Fail(error: .missingLocation).eraseToAnyPublisher()
        }
        
        guard let apiKey = apiKey else {
            return Fail(error: .missingAPIKey).eraseToAnyPublisher()
        }

        guard let taskPublisher = makeRequestPublisher(for: location, using: apiKey, api: .oneCall) else {
            return Fail(error: .apiURLCreation).eraseToAnyPublisher()
        }

        return execute(taskPublisher, maxRetries: maxRetries)
            .decode(type: Forecast.self, decoder: decoder)
            .mapError { error in
                switch error {
                case let serviceError as OpenWeatherAPIServiceError:
                    return serviceError
                case let decodingError as DecodingError:
                    return OpenWeatherAPIServiceError.dataDecodingFailed(decodingError)
                default:
                    return OpenWeatherAPIServiceError.generic(error: error)
                }
            }
            .eraseToAnyPublisher()
    }
}
