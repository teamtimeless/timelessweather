//
//  OpenWeatherAPIService.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/17/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public final class OpenWeatherAPIService {
    public var taskPublisher: SessionDataTaskPublishing
    public var subscriptionQueue: DispatchQueue
    public var apiKey: String?


    // MARK: - Init
    public init(
        session: URLSession = .shared,
        subscriptionQueue: DispatchQueue = DispatchQueue(
            label: OpenWeatherAPIService.defaultSubscriptionQueueLabel,
            qos: .default
        ),
        apiKey: String? = nil
    ) {
        self.taskPublisher = SessionDataPublisher(session: session)
        self.subscriptionQueue = subscriptionQueue
        self.apiKey = apiKey
    }


    public static let defaultSubscriptionQueueLabel = "OpenWeather API Service"


    public static let defaultDecoder: JSONDecoder = {
        let decoder = JSONDecoder()

        // Use Unix time
        decoder.dateDecodingStrategy = .secondsSince1970

        return decoder
    }()
}

extension OpenWeatherAPIService: OpenWeatherAPIServicing {}


// MARK: - Shared Instance
extension OpenWeatherAPIService {
    public static let shared = OpenWeatherAPIService(session: .shared)
}
