//
//  SessionDataTaskPublishing.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


public protocol SessionDataTaskPublishing: class {
    var session: URLSession { get }

    func dataTaskPublisher(for request: URLRequest) -> URLSession.DataTaskPublisher
}


extension SessionDataTaskPublishing {

    func dataTaskPublisher(for request: URLRequest) -> URLSession.DataTaskPublisher {
        session.dataTaskPublisher(for: request)
    }
}
