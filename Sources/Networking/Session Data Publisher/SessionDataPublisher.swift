//
//  SessionDataPublisher.swift
//  TimelessWeather
//
// Created by Brian Sipple on 4/20/20.
// Copyright © 2020 Timeless. All rights reserved.
//

import Foundation


class SessionDataPublisher: SessionDataTaskPublishing {
    var session: URLSession


    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
}
